<?php

namespace Hestec\Country;

use SilverStripe\ORM\DataObject;

class City extends DataObject {

    private static $singular_name = 'City';
    private static $plural_name = 'Cities';

    private static $table_name = 'HestecCity';

    private static $db = array(
        'NameEnglish' => 'Varchar(100)',
        'NameLocal' => 'Varchar(100)',
        'NameDutch' => 'Varchar(100)',
        'SearchNames' => 'MultiValueField',
        'WikiDataId' => 'Int',
        'Latitude' => 'Decimal(10,8)',
        'Longitude' => 'Decimal(11,8)',
        'Timezone' => 'Varchar(50)',
        'Population' => 'Int',
        'Region' => 'Varchar(100)',
        'RegionCode' => 'Varchar(10)',
        'ElevationMeters' => 'Int'
    );

    private static $has_one = array(
        'Country' => Country::class
    );

}