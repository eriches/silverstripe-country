<?php

namespace Hestec\Country;

use SilverStripe\ORM\DataObject;

class Country extends DataObject {

    private static $singular_name = 'Country';
    private static $plural_name = 'Countries';

    private static $table_name = 'HestecCountry';

    private static $db = array(
        'NameEnglish' => 'Varchar(100)',
        'NameLocal' => 'Varchar(100)',
        'NameDutch' => 'Varchar(100)',
        'Code' => 'Varchar(2)',
        'Iso3' => 'Varchar(3)',
        'NumCode' => 'Int',
        'DialCode' => 'Int',
        'SearchNames' => 'MultiValueField',
        'WikiDataId' => 'Int',
        'CurrencyCode' => 'Varchar(3)'
    );

    private static $default_sort='NameDutch';

    private static $has_many = array(
        'Cities' => City::class
    );

}