<?php

use SilverStripe\Core\Config\Config;
use Hestec\Country\City;
use Hestec\Country\Country;

class CountryWebhook extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'Country',
        'City',
        'test'
    );

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            /*$whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://api-internet.whitelabeled.nl/v1/compare/'.$whitelabel_id.'?include_tv=true&include_phone=true&page=1&items_per_page=1', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);
            $msgs = json_decode($response->getBody());


            foreach ($msgs->products as $node) {

                //echo $node->score_elements;

                foreach ($node->score_elements as $node) {

                    echo $node->type;

                }

            }*/

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function Country() {

        $Params = $this->getURLParams();

        if (isset($Params['ID']) && is_numeric($Params['ID'])) {

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', 'https://api.hestec.xyz/CountryApi/Country/'.$Params['ID'], [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ]
                ]);

            } catch (Exception $e) {
                //echo 'Caught exception: ', $e->getMessage(), "\n";
                //$email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Whitelabeled Energy", $e->getMessage());
                //$email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody(), true);

                if (!empty($msgs)) {

                    $country = Country::get()->byID($Params['ID']);

                    if (empty($city)) {

                        $country = new Country();
                        $country->ID = $msgs['ID'];

                    }

                    if (isset($msgs['NameEnglish'])){
                        $country->NameEnglish = $msgs['NameEnglish'];
                    }
                    if (isset($msgs['NameLocal'])) {
                        $country->NameLocal = $msgs['NameLocal'];
                    }
                    if (isset($msgs['NameDutch'])) {
                        $country->NameDutch = $msgs['NameDutch'];
                    }
                    if (isset($msgs['SearchNamesValue'])) {
                        $country->SearchNames = $msgs['SearchNamesValue'];
                    }
                    if (isset($msgs['WikiDataId'])) {
                        $country->WikiDataId = $msgs['WikiDataId'];
                    }
                    if (isset($msgs['Code'])) {
                        $country->Code = $msgs['Code'];
                    }
                    if (isset($msgs['Iso3'])) {
                        $country->Iso3 = $msgs['Iso3'];
                    }
                    if (isset($msgs['NumCode'])) {
                        $country->NumCode = $msgs['NumCode'];
                    }
                    if (isset($msgs['DialCode'])) {
                        $country->DialCode = $msgs['DialCode'];
                    }
                    if (isset($msgs['CurrencyCode'])) {
                        $country->CurrencyCode = $msgs['CurrencyCode'];
                    }

                    $country->write();

                    return "updated";

                }
                return "no data";

            }

            return "error";

        }
        return "no ID";

    }

    public function City() {

        $Params = $this->getURLParams();

        if (isset($Params['ID']) && is_numeric($Params['ID'])) {

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', 'https://api.hestec.xyz/CountryApi/City/'.$Params['ID'], [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ]
                ]);

            } catch (Exception $e) {
                //echo 'Caught exception: ', $e->getMessage(), "\n";
                //$email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Whitelabeled Energy", $e->getMessage());
                //$email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody(), true);

                if (!empty($msgs)) {

                    $city = City::get()->byID($Params['ID']);

                    if (empty($city)) {

                        $city = new City();
                        $city->ID = $msgs['ID'];

                    }

                    if (isset($msgs['NameEnglish'])) {
                        $city->NameEnglish = $msgs['NameEnglish'];
                    }
                    if (isset($msgs['NameLocal'])) {
                        $city->NameLocal = $msgs['NameLocal'];
                    }
                    if (isset($msgs['NameDutch'])) {
                        $city->NameDutch = $msgs['NameDutch'];
                    }
                    if (isset($msgs['SearchNamesValue'])) {
                        $city->SearchNames = $msgs['SearchNamesValue'];
                    }
                    if (isset($msgs['WikiDataId'])) {
                        $city->WikiDataId = $msgs['WikiDataId'];
                    }
                    if (isset($msgs['Latitude'])) {
                        $city->Latitude = $msgs['Latitude'];
                    }
                    if (isset($msgs['Longitude'])) {
                        $city->Longitude = $msgs['Longitude'];
                    }
                    if (isset($msgs['Timezone'])) {
                        $city->Timezone = $msgs['Timezone'];
                    }
                    if (isset($msgs['Population'])) {
                        $city->Population = $msgs['Population'];
                    }
                    if (isset($msgs['Region'])) {
                        $city->Region = $msgs['Region'];
                    }
                    if (isset($msgs['RegionCode'])) {
                        $city->RegionCode = $msgs['RegionCode'];
                    }
                    if (isset($msgs['ElevationMeters'])) {
                        $city->ElevationMeters = $msgs['ElevationMeters'];
                    }
                    if (isset($msgs['CountryID'])) {
                        $city->CountryID = $msgs['CountryID'];
                    }

                    $city->write();

                    return "updated";

                }
                return "no data";

            }

            return "error";

        }
        return "no ID";

    }

}
